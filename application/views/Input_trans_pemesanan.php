<!DOCTYPE html>
<html>
<head>
	<title>Toko Pizza</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>

	<header class="header">
    <h1 class="judul" align="center">Toko Pizza</h1>
    
        <div class="menu">
    <ul>
    <li><a href="#">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>index.php/Master_karyawan/listmasterkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>index.php/Master_menu/listmastermenu">Data Menu</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="<?=base_url();?>index.php/Trans_pemesanan/listtranspemesanan">Pemesanan</a></li>
        </ul>
    </li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>DATA PEMESANAN</b><br>
                </div>
            </div>
   
    <form action="<?=base_url()?>index.php/Trans_pemesanan/input" method="post">
<table width="1000px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00CCFF">
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <select name="nik" id="nik">
      <?php foreach($data_master_karyawan as $data) {?>
      	<option value="<?= $data->nik; ?>"><?= $data->nama; ?></option>
      <?php } ?>
      </select>
    </td>
  </tr>
  <tr>
    <td>Nama Pelanggan</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_pelanggan" id="nama_pelanggan" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Nama Menu</td>
    <td>:</td>
    <td>
      <select name="kode_menu" id="kode_menu">
      <?php foreach($data_master_menu as $data) {?>
      	<option value="<?= $data->kode_menu; ?>"><?= $data->nama_menu; ?></option>
      <?php } ?>
      </select>
    </td>
  </tr>
  <tr>
    <td>Qty</td>
    <td>:</td>
    <td><input type="text" name="qty" id="qty" /></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Proses">
    <input type="reset" name="reset" id="reset" value="Reset">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>index.php/Trans_pemesanan/listtranspemesanan">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>