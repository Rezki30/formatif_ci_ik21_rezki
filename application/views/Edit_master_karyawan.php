<!DOCTYPE html>
<html>
<head>
	<title>Toko Pizza</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>

	<header class="header">
    <h1 class="judul" align="center">Toko Pizza</h1>
    
        <div class="menu">
    <ul>
    <li><a href="#">Home</a></li>
    <li class="dropdown"><a href="#">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>index.php/Master_karyawan/listmasterkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>index.php/Master_menu/listmastermenu">Data Menu</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="#">Transaksi</a>
    	<ul class="isi-dropdown">
        	<li><a href="#">Pemesanan</a></li>
        </ul>
    </li>
    <li><a href="#">Log ut</a></li>
    </ul>
    </div>
    </header>
    <br/>
       
        <div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>EDIT DATA KARYAWAN</b><br>
                </div>
            </div>
<?php
	foreach ($detail_karyawan as $data) {
		$nik			= $data->nik;
		$nama			= $data->nama;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
		$tempat_lahir	= $data->tempat_lahir;
		$tgl_lahir		= $data->tanggal_lahir;
	}
	//Pisah bulan, tanggal, tahun
	$tahun_pisah = substr($tgl_lahir, 0, 4);
	$bulan_pisah = substr($tgl_lahir, 5, 2);
	$tanggal_pisah = substr($tgl_lahir, 8, 2);
?>
    <form action="<?=base_url()?>index.php/master_karyawan/edit/<?=$nik;?>" method="post">

<table width="1000px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#CCCCCC">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td>
      <input type="text" name="nik" id="nik" value="<?=$nik;?>" maxlength="20" readonly>
    </td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <input type="text" name="nama" id="nama" value="<?=$nama;?>" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"><?=$alamat;?></textarea></td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value="<?=$telp?>" /></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input type="text" name="tempat_lahir" id="tempat_lahir" value="<?=$tempat_lahir?>" /></td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
   	 <select name="tgl" id="tgl">
     <?php
     	for($tgl_pisah=1;$tgl_pisah<=31;$tgl_pisah++){
			$select_tgl = ($tgl_pisah == $tanggal_pisah) ? 'selected' : '';
	 ?>
     	<option value="<?=$tgl_pisah;?>" <?=$select_tgl;?>><?=$tgl_pisah;?></option>
     <?php
		}
	 ?>
     </select>
      <select name="bln" id="bln">
      <?php
       $bulan_n = array('','Januari','Februari','Maret','April',
	   					'Mei','Juni','Juli','Agustus','September',
						'Oktober','November','Desember');
		for($bln=1;$bln<=12;$bln++){
			$select_bln = ($bln == $bulan_pisah) ? 'selected' : '';
	  ?>
      <option value="<?=$bln;?>" <?=$select_bln;?>><?=$bulan_n[$bln];?> </option>
      <?php
		}
	  ?>
      </select>
      <select name="thn" id="thn">
      <?php
      	for($thn = 1990; $thn <= date('Y');$thn++){
			$select_thn = ($thn == $tahun_pisah) ? 'selected' : '';
	  ?>
      	<option value="<?=$thn;?>" <?=$select_thn;?>><?=$thn;?></option>
      <?php
		}
	  ?>
      </select>

    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>index.php/master_karyawan/listmasterkaryawan">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
  </form>
</table>
</div>
</body>
</html>