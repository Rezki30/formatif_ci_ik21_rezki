<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Trans_pemesanan_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("menu_model");
    }

	//panggil nama table
	private $_table = "transaksi_pemesanan";
	
	public function tampilDataTransaksiPemesanan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataTransaksiPemesanan2()
	{
		$query = $this->db->query(
			"SELECT A. *,  B.nama, C.nama_menu FROM " . $this->_table . " AS A
			INNER JOIN `master_karyawan` AS B ON A.nik = B.nik
			INNER JOIN `master_menu` AS C ON A.kode_menu = C.kode_menu");
			$data = $query->result();
			
			
		return $query->result();
	
	}
	
	public function tampilDataTransaksiPemesanan3()
	{
		$this->db->select('*');
		$this->db->order_by('id_pemesanan', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function savepemesanan($id)
	{
		$pegawai            = $this->input->post('nik');
        $nama_pelanggan     = $this->input->post('nama_pelanggan');
        $kode_menu          = $this->input->post('kode_menu');
        $qty                = $this->input->post('qty');
		$hargamenu          = $this->menu_model->cariHargaMenu($kode_menu);
		
		$data['id_pemesanan']	= $id;
		$data['nik']			= $pegawai;
		$data['tgl_pemesanan']	= date('Y-m-d');
		$data['nama_pelanggan']	= $nama_pelanggan;
		$data['kode_menu']		= $kode_menu;
		$data['qty']			= $qty;
		$data['total_harga']	= $qty * $hargamenu;
		
		$this->db->insert($this->_table, $data);
	}
}
