<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_menu extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("menu_model");
	}
	
	public function index()
	{
		$this->listmastermenu();
	}
	
	public function listmastermenu()
	{
		$data['data_master_menu'] = $this->menu_model->tampilDataMasterMenu();
		$this->load->view('home_menu', $data);
	}
	
	public function input()
	{
		if (!empty($_REQUEST)) {
			$m_menu = $this->menu_model;
			$m_menu->save();
			redirect("index.php/master_menu/listmastermenu", "refresh");
		}
		$this->load->view('input_master_menu');
	}
	
	public function delete($kode_menu)
	{
		$m_menu = $this->menu_model;
		$m_menu->delete($kode_menu);
		redirect("index.php/master_menu/listmastermenu", "refresh");
	}

	public function edit($kode_menu)
	{
		$data['detail_menu'] = $this->menu_model->detail($kode_menu);
		
		if (!empty($_REQUEST)) {
			$m_menu = $this->menu_model;
			$m_menu->update($kode_menu);
			redirect("index.php/master_menu/listmastermenu", "refresh");
		}
		$this->load->view('edit_master_menu', $data);
	}

}